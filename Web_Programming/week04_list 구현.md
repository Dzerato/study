list - regist web 구조
----------------------

---

### list-regist

![List-regist Web Proccess](https://cdn.discordapp.com/attachments/555042542262026262/562709538428420096/1.png)

![List-regist Web Directory](https://cdn.discordapp.com/attachments/555042542262026262/562709541905498112/2.png)

---

### list.java

```
package kr.ac.bu.lecture.servlet;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.ac.bu.lecture.domain.Lecture;
import kr.ac.bu.lecture.service.LectureService;
import kr.ac.bu.lecture.service.logic.LectureServiceLogic;


@WebServlet("/list.do")
public class ListServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LectureService service = new LectureServiceLogic();
```

```
        List<Lecture> lectures = service.findAll();
```

> LectureServiceLogin의 findAll()메소드를 사용하여  
> 저장된 모든 데이터를 lectuures 리스트 객체에 저장한다.

```
        //lectures의 데이터를 lectures라는 이름으로 request에 저장
        request.setAttribute("lectures", lectures);
```

> 요청값에 lectures라는 이름으로 lectures 객체의 데이터를 추가한다.  
> \** setAttribute

```
        request.getRequestDispatcher("views/list.jsp").forward(request, response);
```

> views/list.jsp에게 Dispatch 형식으로 request  
> 기억 데이터 : 이전 request (/list.do)  
> \** Dispatch

```
    }
}

```

### list.jsp

```
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx">${pageContext.request.contextPath }</c:set>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>강좌목록</title>
<link href="${ctx }/resources/css/bootstrap.min.css" rel="stylesheet">
<link href="${ctx }/resources/css/style.css" rel="stylesheet">
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3>강좌 목록</h3>
                <table class="table table-hover table-condensed">
                    <colgroup>
                        <col width="80" align="center">
                        <col width="100">
                        <col width="30%">
                        <col width="*">
                        <col width="100">
                        <col width="100">
                    </colgroup>
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>강좌ID</th>
                            <th>강좌명</th>
                            <th>강사</th>
                            <th>UPDATE</th>
                            <th>DELETE</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:choose>
                            <c:when test="${lectures eq null || empty lectures}">
                                <tr>
                                    <td colspan="6" align="center">등록된 강좌가 없습니다.</td>
                                </tr>
                            </c:when>
                            <c:otherwise>

```

```
                                <c:forEach items="${lectures }" var="lectures" varStatus="status">
                                    <tr>
                                        <td>${Status.count }</td>
                                        <td>${lecture.id }</td>
                                        <td><a href="detail.html">JSP와 Servlet</a></td>
                                        <td>${lecture.instructor }</td>
                                        <td><a class="btn btn-xs btn-warning" href="modify.html">UPDATE</a></td>
                                        <td><a class="btn btn-xs btn-danger" href="#">DELETE</a></td>
                                    </tr>
                                </c:forEach>

```

> lecture HashMap에 저장된 각 값들을 불러와 table형식으로 출력

```
                            </c:otherwise>
                        </c:choose>
                    </tbody>
                </table>
                <a class="btn btn-sm btn-success" href="register.do">강좌등록</a>
            </div>
        </div>
    </div>
</body>
</html>
```

> \** EL 태그

### LectureStoreLogic.java

```
private static LectureStoreLogic uniqueInstance;

private Map<String, Lecture> lectureRepo;

private LectureStoreLogic() {
  this.lectureRepo = new HashMap<String, Lecture>();
}

public static LectureStoreLogic getInstace() {
  if(uniqueInstance == null) {
    uniqueInstance = new LectureStoreLogic();
  }
  return uniqueInstance;
}
```

> \** SingleTon Pattern

---

### setAttribute / getAttribute

##### setAttribute

-	A.setAttribute(String name, Object value)  
-	세션의 함수, 세션(A)에 이름(name)에 값(value)를  
	추가/수정(이름이 중복되는 경우)하여 값을 전달  
	`
	request.setAttribute("lectures1", lectures2);
	` \`  
	name : lectures1 / value:lectures2

##### getAttribute

-	A.getAttribute(String name)
-	세션(A)에서 해당 이름(name)의 값의 데이터를 읽어옴

---

### Redirect / Dispatch

![Redirect / Dispatch](https://cdn.discordapp.com/attachments/555042542262026262/562709535505121291/redirect_dispatch.PNG)

##### Redirect

-	Client에서 재요청 처리(Client에게 요청을 넘길 URL을 전달)
-	별도의 request는 전달하지 않음(페이지 이동)
-	이동 후 request는 별도의 객체  
	`resp.sendRedirect(req.getContextPath() + "/views/registerForm.jsp");`

##### Dispatch

-	Server 내에서 처리 후 전달  
	`request.getRequestDispatcher("views/list.jsp").forward(request, response);`  
	views/list.jsp에게 전달(request - /list.do 저장)
-	브라우저의 주소창이 바뀌지 않음
-	Servlet은 다른 컴포넌트에게 요청하여 처리

---

### EL / JSTL 태그

##### EL(Expression language)

-	JSP의 기본 객체의 속성 사용  

| Tag         | text                              |
|-------------|-----------------------------------|
| pageContext | JSP페이지 처리시 사용             |
| request     | HTTP 요청 처리 시 사용            |
| session     | 웹 브라우저 요청 처리 시 사용     |
| application | 웹 어플리케이션 요청 처리 시 사용 |

변수 선언 : `${test}`  
속성값 호출 : `${hello.test} = ${hello['test'] = ${hello["test"]}`

-	자바 Class의 Method 호출

```
<%
   Thermometer th = new Thermometer();
   request.setAttribute("t", th);
%>

${t.setCelsius('서울', 27.3)}
${t.info}
```

> EL 표현언어는 only 메서드만 호출한다.

-	표현 언어만의 기본 객체 제공

| Tag       | text                                               |
|-----------|----------------------------------------------------|
| pagescope | pageContext에 저장된 속성과 그 값을 Mapping한 객체 |
| param     | 요청 파라미터의 파라미터 명과 값을 Mapping한 객체  |
| cookie    | 쿠키 이름과 값을 Mapping한 객체                    |

`${Cooke.id.value}`

> 1.	Cookie가 null이면 null 리턴  
> 2.	아니라면 id 검사 (null이면 null 리턴)  
> 3.	아니면 value 검사  
> 	※ null 또한 공백으로 출력

-	연산자
	-	비교 연산자 : eq(==) / ne(!=)
	-	논리 연산자 : and(&&), or(||), not(!)
	-	empty 연산 : 값이 null / 공백 / 길이가 0 인 경우 true

##### JSTL(Jsp Standard Tag Library)

-	선언 기반  
	`<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>`  
	> 핵심 라이브러리 추가(core 태그 추가)

-	기본 기능 태그

```
<c:out value="value" escapeXml="TRUE|FALSE">
text
</c:out>
```

> escapeXml의 속성값이 False인 경우 or value의 값이 공백인 경우 text 출력  
> value란 이름으로 저장된 값을 출력함

| 속성      | 설명                 | 기본값 |
|-----------|----------------------|--------|
| value     | 출력할 데이터        | \-     |
| default   | 출력할 데이터 기본값 | body   |
| escapeXml | 특수 XML문자 escape  | true   |

```
<c:set value="value" target="target" property="name"/>
```

> target 객체에 value 값을 추가

| 속성     | 설명                      | 기본값 |
|----------|---------------------------|--------|
| value    | 저장할 값                 | body   |
| target   | 수정할 변수명             | \-     |
| property | 수정할 속성               | \-     |
| var      | 값을 저장할 변수명        | \-     |
| scope    | 정보를 저장할 변수의 범위 | Page   |

```
<c:remove var="varName"/>
```

| 속성  | 설명                | 기본값 |
|-------|---------------------|--------|
| var   | 제거할 변수명       | \-     |
| scope | 제거 할 변수의 범위 | all    |

-	조건 처리 태그

```
    <c:choose>
     <c:when test="testCondition">
    내용
     </c:when>
    ...
  when 태그는 복수 사용 가능
    ...
     <c:otherwise>
      내용
     </c:otherwise>
    </c:choose>
```

> when : if문(else if)과 유사  
> otherwise : else 유사

※ when

| 속성 | 설명 | 기본값 |  
|------|---------------|--------|  
| test | 조건 | \- |

-	반복 처리 태그

```
<c:forEach items="${lectures }" var="lectures" varStatus="status">
  <tr>
                  <td>${Status.count }</td>
                  <td>${lecture.id }</td>
                  <td><a href="detail.html">JSP와 Servlet</a></td>
                  <td>${lecture.instructor }</td>
                  <td><a class="btn btn-xs btn-warning" href="modify.html">UPDATE</a></td>
                  <td><a class="btn btn-xs btn-danger" href="#">DELETE</a></td>
              </tr>
</c:forEach>
```

| 속성      | 설명             | 기본값    |
|-----------|------------------|-----------|
| items     | 반복할 item      | \-        |
| begin     | 시작 번호        | 0         |
| end       | 마지막 번호      | Last Data |
| step      | 번호 증가량      | 1         |
| var       | 변수명           | \-        |
| varStatus | 반복 상태 변수명 | \-        |

---

### SingleTon Pattern

-	단 하나의 객체만을 생성하게 강제하는 패
-	인스턴스가 1개만 생성
-	getInstace메소드를 통해 모든 Client에게 동일한 인스턴스를 반환

```
private static LectureStoreLogic uniqueInstance;
```

> private로 인하여 인스턴스 생성을 통한 메소드 호출이 불가능  
> 클래스 내부에서는 자신의 인스턴스를 지님  
> static을 통해 외부에선 객체를 생성하지 않은체 클래스의 인스턴스에 직접 접근함

```
private Map<String, Lecture> lectureRepo;
```

```
private LectureStoreLogic() {
  this.lectureRepo = new HashMap<String, Lecture>();
}
```

> private로 생성자를 생성함으로서 외부에서 해당 생성자를 통한 인스턴스 생성이 불가능

```
public static LectureStoreLogic getInstace() {
```

> Method 는 보통 클래스에 의존적인 성격을 지님(인스턴스를 통해서만 호출 가능)  
> getInstace 메소드는 정적(static) 메소드이므로 해당 클래스에서 바로 호출이 가능

```
  if(uniqueInstance == null) {
```

> 하나의 객체를 가져야 하므로, 객체 존재 여부를 판단함

```
    uniqueInstance = new LectureStoreLogic();
```

> 동일한 클래스 내 이므로 new 생성자를 사용하여 객체를 만들어 반환

```
  }
  return uniqueInstance;
}
```
