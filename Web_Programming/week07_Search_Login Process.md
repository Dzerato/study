Search & Login Process
----------------------

---

### Search Process

![Search Process](https://gitlab.com/Dzerato/study/uploads/140363c9717b5cc611e2cec7302471ee/SearchProcess.png)

-	list.jsp

```
<form action="search.do" method="post">
<input name = "name" type="text" placeholder="곡명을 입력하세요."> <input class="btn btn-xs btn-default" type="submit" value="검색">
</form>
```

> 입력한 값을 name이라는 이름의 파라미터로 search.do(SearchServlet.jsp)로 전달(Get방식)

-	SearchServlet.java

```
        request.setCharacterEncoding("UTF-8");
```

> 파라미터에 저장된 값이 한글인 경우 encoding 문제가 발생하므로 request 의 encoding을 변환

```
        MusicService service = new MusicServiceLogic();
        List<Music> list = service.findByName(request.getParameter("name"));

```

> name 파라미터의 값을 이용하여 MusicJdbcStore에서 DB에 접근

```

        request.setAttribute("musicList", list);
        request.getRequestDispatcher("list.jsp").forward(request, response);
    }
}

```

> MusicJdbcStore에서 반환 값을 request에 추가하여 list.jsp로 반환

-	MusicServiceLogic.java

```
    @Override
    public List<Music> findByName(String name) {
        return store.readByName(name);
    }

```

-	MusicJdbcStore.java

```
   @Override
   public List<Music> readByName(String name) {
      String sql = "SELECT ID, NAME, ARTIST_NAME, ALBUM_TITLE, IMAGE, AGENT_NAME FROM MUSIC_TB"
            + " WHERE NAME LIKE ?";

      Connection conn = null;
      PreparedStatement pstmt = null;
      ResultSet rs = null;
      List<Music> list = new ArrayList<Music>();

      try {

         conn = factory.createConnection();
         pstmt = conn.prepareStatement(sql);
         pstmt.setString(1, "%" + name + "%");
         rs = pstmt.executeQuery();

         while(rs.next()) {
            Music music = new Music();
            music.setId(rs.getInt("ID"));
            music.setName(rs.getString("NAME"));
            music.setArtist(rs.getString("ARTIST_NAME"));
            music.setAlbum(rs.getString("ALBUM_TITLE"));
            music.setImage(rs.getString("IMAGE"));
            music.setAgent(rs.getString("AGENT_NAME"));
            list.add(music);
         }

      } catch (SQLException e) {
         e.printStackTrace();
      } finally {
         JdbcUtils.close(conn, pstmt, rs);
      }
      return list;
   }
```

> 파라미터로 전달받은 검색 키워드를 중심으로 sql문 실행  
> % : 와일드카드 - 어떤 하나 이상의 문자 사용(없어도 가능)  
> 검색 결과를 list(list형)에 저장하여 반환

### Detail Process

![Detail Process](https://gitlab.com/Dzerato/study/uploads/40f5cc76be938bfbeff5a99f6326fa2a/DetailProcess.png)

-	list.jsp

```
<td><span class="spanTitle">${music.name }</span><a class="btn btn-xs btn-default" href="detail.do?musicId=${music.id }"><b>i</b></a></td>

```

> DB상에서 불러온 음악 리스트 중 각 리스트별 id를 기준으로 이동  
> detail.do에 musicId 파라미터 값을 추가하여 전달(Get 방식)

-	DetailServlet.java

```
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

```

> list.jsp에서 Get방식으로 전달

```
            MusicService service = new MusicServiceLogic();
        Music music = service.find(Integer.parseInt(request.getParameter("musicId")));

```

> list.jsp 에서 musicId 파라미터로 전달 해온 값(String)을 Int형으로 형변환  
> 변환된 값을 MusicServiceLogic의 find 메소드를 통해 MusicJdbcStore 클래스에서 DB검색 실시

```
        request.setAttribute("music", music);
        request.getRequestDispatcher("detail.jsp").forward(request, response);

    }
}

```

> 검색된 결과 값(music 객체)을 요청에 추가하여 detail.jsp에 전달

-	MusicServiceLogic.java

```
public class MusicServiceLogic implements MusicService {

    private MusicStore store;

    public MusicServiceLogic() {
        store = new MusicJdbcStore();
        }

    @Override
    public Music find(int id) {
        return store.read(id);
    }

```

-	MusicJdbcStore.java

```
   @Override
   public Music read(int id) {
      String sql = "SELECT ID, NAME, ARTIST_NAME, ALBUM_TITLE, IMAGE, AGENT_NAME FROM MUSIC_TB WHERE ID = ?" ;
      Connection conn = null;
      PreparedStatement pstmt = null;
      ResultSet rs = null;
      Music music = null;

      try {
         conn = factory.createConnection();
         pstmt = conn.prepareStatement(sql);
         pstmt.setInt(1, id);
         rs= pstmt.executeQuery();

         if(rs.next()) {
            music = new Music();
            music.setId(rs.getInt("ID"));
            music.setName(rs.getString("NAME"));
            music.setArtist(rs.getString("ARTIST_NAME"));
            music.setAlbum(rs.getString("ALBUM_TITLE"));
            music.setImage(rs.getString("IMAGE"));
            music.setAgent(rs.getString("AGENT_NAME"));
         }
      } catch (SQLException e) {
         e.printStackTrace();
      }finally {
         JdbcUtils.close(conn,rs,pstmt);
      }
      return music;
   }

}

```

> list.jsp에서 musicId 파라미터로 전달받은 id값을 기준으로 검색  
> 검색 결과의 각 항목을 music객체에 저장하여 전달

-	detail.jsp

```

<body>
<%@ include file="header.jspf" %>
...
    <h2>${music.name }</h2>
    <hr>
    <table id="musicDetail">
        <colgroup>
            <col width="200px">
            <col width="*">
        </colgroup>
        <tr>
            <td><img src="resources/img/${music.image }" width="180px"></td>
            <td>
            <table class="table">
                    <colgroup>
                        <col width="150">
                        <col width="*">
                    </colgroup>
                    <tr>
                        <th>곡명</th>
                        <td>${music.name }</td>
                    </tr>
                    <tr>
                        <th>앨범명</th>
                        <td>${music.album }</td>
                    </tr>
                    <tr>
                        <th>아티스트</th>
                        <td>${music.artist }</td>
                    </tr>
                    <tr>
                        <th>기획사</th>
                        <td>${music.agent }</td>
                    </tr>
...
</body>

```

### Login Process

![Login Process](https://gitlab.com/Dzerato/study/uploads/0277e33e218c89dee11f343455561149/LoginProcess.png)

-	list.jsp / detail.jsp

```
 <%@ include file="header.jspf" %>

```

> header.jspf 파일을 불러와 해당 JSP페이지에 삽입

-	header.jspf

```
<c:choose>
    <c:when test='${loginedUser eq null }'>
        <a href="login.do">Login</a> || <a href="join.do">Join</a>    
    </c:when>

```

> loginedUser : 로그인한 객체  
> 해당 객체가 null인지에 따라 판별  
> a 태그 클릭시 링크 이동(Get 방식)  
> null인 경우 아래 형태로 출력

![Login1](https://gitlab.com/Dzerato/study/uploads/d06fc9960de0dd5dc58bcce7a3581501/login1.PNG)

```
<c:otherwise>
  <b>${loginedUser.name }</b>님 반갑습니다. [<a href="logout.do">LogOut</a>]
</c:otherwise>
</c:choose>

```

> loginedUser 객체가 null이 아닌 경우  
> 해당 객체의 name 값을 불러와 아래와 같은 형태로 출력

![Login2](https://gitlab.com/Dzerato/study/uploads/c7b406b0bfdf65a3d93969a1de687026/login2.PNG)

-	loginServlet.java

```
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("login.jsp");
    }

```

> a태그를 통해 Get방식으로 들어온 경우 login.jsp로 이동

```
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

```

> login.jsp의 Form 태그의 post 방식으로 접근 시 실행

```

  UserService service = new UserServiceLogic();

  User user = new User();
  user.setLoginId(request.getParameter("loginId"));
  user.setPassword(request.getParameter("passworkd"));
  User loginedUser = service.login(user);

```

> user : login.jsp에서 넘어온 데이터가 저장된 객체  
> loginedUser : DB에서 user객체의 id로 검색한 결과가 저장된 객체

```
  if(loginedUser != null) {
    HttpSession session = request.getSession();
    session.setAttribute("loginedUser",loginedUser);
  }else {
    HttpSession session = request.getSession(false);
    session.invalidate();
  }

  response.sendRedirect("list.do");
}

```

> loginedUser 객체의 값을 확인 하여 동작  
> null이 아닌 경우 : loginedUser 객체 정보를 session에 저장  
> null인 경우 : 세션을 종료(invalidate)함

-	login.jsp

```
    <form action="login.do" method="post" >
        <table class="table">
            <tr>
                <th>ID</th>
                <td><input id="loginId"  name="loginId" class="form-control" type="text" value="" placeholder="ID를 입력해주세요."></td>
            </tr>
            <tr>
                <th>Password</th>
                <td><input id="password" name="password" class="form-control" type="password" value="" placeholder="비밀번호를 입력해주세요."></td>
            </tr>
        </table><br>
        <div align="center"><input class="btn" type="reset" value="취소"> <input class="btn btn-success" type="submit" value="로그인"></div>
    </form>

```

> 입력한 id와 password를 loginId, password 값으로 login.do(loginServlet.java)로 전달

-	UserServiceLogic.java

```
public class UserServiceLogic implements UserService {

    private UserStore store;

    public UserServiceLogic() {
        store = new UserJdbcStore();
    }

    @Override
    public User login(User user) {
        return store.read(user.getLoginId());
    }
...
}

```

> DB에서 user 정보 접근 및 읽어오기 위한 store 객체 생성

-	UserJdbcStore.java

```
    @Override
    public User read(String id) {

        String sql = "SELECT LOGINID, PASSWORD, NAME FROM USER_TB WHERE LOGINID = ?";

        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        User user = null;

        try {
            conn = factory.createConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, id);

            rs = pstmt.executeQuery();

```

> login.jsp에서 입력한 id값을 기준으로 DB 검색 실시

```
            if(rs.next()) {
                user = new User();
                user.setLoginId(rs.getString(1));
                user.setPassword(rs.getString(2));
                user.setName(rs.getString(3));
            }
```

> 검색된 값의 Id, Password, Name을 user 객체에 저장

---

### session

-	HTTP는 비연결형 프로토콜
-	사용자에게 지속적으로 연결된 듯한 형태를 제공하기 위한 정보를 기억하는 공간의 필요
-	Session Token
	-	Client를 식별하기 위한 정보로서 Server에서 생성, 전달
	-	인증관련 정보는 Client와 Server 모두 저장
	-	대표적으로 WAS의 Hash
-	Token 저장 방식
	-	Hidden Field : Form Tag의 Hidden 속성 값에 저장  
		 소스 보기를 통해 쉽게 노출
	-	URL Rewriting : URL상에 세션 정보를 포함(Get방식 / Cookie를 사용하지 않는 경우)
	-	Cookie : Client 측에 특정 정보를 저장하기 위한 HTTP 프로토콜 기술
		-	http response/request를 통해 Server 와 Client 사이의 통신

---

### Wrapper class

-	기본타입의 데이터를 객체로 취급해야하는 경우에 사용  
	ex) 메소드의 인수로 객체 타입 요구시, 기본 타입 데이터를 객체로 변환 후 사용
-	9개의 기본 타입에 해당하는 데이터를 객체로 포장해주는 클래스

| 기본 타입 | 래퍼 클래스 |
|-----------|-------------|
| byte      | Byte        |
| short     | Short       |
| int       | Integer     |
| long      | Long        |
| float     | Float       |
| double    | Double      |
| char      | Character   |
| boolean   | Boolean     |

-	Boxing : 기본 타입 데이터 -> 래퍼 클래스의 인스턴스  
	UnBoxing : 래퍼 클래스의 인스턴스 -> 기본 타입 데이터

```
// Boxing, UnBoxing
Integer num = new Integer(17);
int n = num.intValue();

// Auto Boxing, Auto UnBoxing
Character ch = 'X';
char c = ch;
```

-	JDK 1.5 부터는 자동으로 Boxing/UnBoxing 지원
