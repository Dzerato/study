## Git에 관하여
---
#### 1. Git이란?
* 버전관리 시스템(**VCD**; Version Control System)  
소스코드 관리(**SCM**; Source Code Management)   
* 분산형 관리 시스템 형태의 Workflow 제공  
    ※ 분산형 관리 시스템 : C/S 구조와 달리 개별적인 로컬 저장소를 갖고, 작업하는 형태
* 무료이자 공개 소프트웨어이며 다양한 프로그램에 연동 가능

---

### 2. Git 기본 사용
* 최초 정보 입력  
 `git config --global user.name "[이름]"`  
 `git config --global user.email "[이메일]"`
* 로컬 저장소 내 [ **작업 디렉토리(Working Directory)** -> **인덱스(Index)** -> **Head** ] 구조로 작업이 진행
* 새로운 저장소 생성 : 클라이언트의 로컬 저장소를 생성  
    `git init` 
* 저장소 받아오기 : 로컬 / 원격 서버의 저장소를 특정 로컬 저장소에 복제  
    `git clone [경로]`  
    ※ 변경이력을 포함하여 복제
* 원격 저장소 설정  
    `git remote add [원격저장소별명] [주소]`  
    ※ 변경이력을 포함하여 복제
* **작업 디렉토리** 내 수정 내역을 **인덱스**에 추가  
    `특정 파일을 추가 : git add [파일명]`  
    `디렉토리 내 모든 파일 추가 : git add *`
* **작업 디렉토리** 내 수정 내역을 확정하며, **HEAD**에 반영  
    `git commit -m "[메세지]"`
* 로컬 저장소의 변경 내역을 원격 서버로 업로드  
    `git push [원격저장소별명] [branch명]`   
    `git remote add [원격저장소별명] [원격 서버 주소]`
    
---
    
#### 3. Git 응용 사용
* Branch
 * 로컬 저장소 생성 시 **master** branch 생성
 * 특정 작업 시 분리된 작업환경(새로운 Branch)을 위해 사용
 * 새로운 가지 생성  
  `git checkout -b [branch명]`
 * 특정 가지로 이동  
  `git checkout [branch명]`
 * 특정 가지 삭제  
  `git branch -d [branch명]`
 * 작업한 가지를 업로드 (업로드 전까지 다른 사용자의 접근 불가)  
  `git push [원격저장소별명] [branch명]`
   
* 갱신과 병합
 * 로컬 저장소를 원격 저장소에 맞춰 갱신  
  `git pull`  
  ※ 원격 저장소의 내용이 로컬 저장소에 받아지고(**Fetch**), 병합(**Merge**) 됨
 * 다른 가지의 내용을 현재 가지에 병합  
  `git merge [Branch명]`
 * 병합 전, 변경 내용 확인  
  `git diff [원래 Branch명] [비교 Branch명]`
  
* 데이터 복구
 * 로컬 변경 내용 복구  
 `git checkout -- [파일명]`  
   * 변경 전 상태(Head)로 복구
   * 이미 인덱스에 추가된 내용 / 신규 파일은 유지
 * 원격 저장소 데이터로 복구
   * 원격 저장소의 최신 데이터 획득  
    `git fetch [원격저장소별명]`
   * 로컬저장소의 master 가지의 지칭 위치 변경  
    `git reset --hard [원격저장소별명]/master`
  
* 꼬리표(Tag)
 * 확정본 식별자 획득  
  `git log`
 * 꼬리표 부착  
  `git tag 1.0.0 [확정본 식별자]`

---
 
 #### 4. 기능 비교
 * 소스 릴리즈(Push, pull, fetch)  
  * Fetch  
    * 원격 저장소의 파일을 로컬 저장소로 다운로드 받음
    * 파일을 수정하지 않고, 다운로드 받기만 함
  * Pull
    * `fetch + merge [Branch]` 와 동일한 작업을 수행
    * 원격 저장소의 파일을 다운받고, Branch를 병합
  * Push
    * 로컬 저장소(HEAD)의 저장값을 원격 저장소로 업로드함
    * `git push -u [원격저장소별명] [Branch명]`  
      **-u** = **--set-upstream** : 현재 Branch에 대해 매번 Remote와 Branch 값을 생략해도 구동하도록 실행
 * 소스 병합(Merge, Rebase)
    * Merge
        * Master Branch의 최신 Commit과 Slave Branch의  
        최신 Commit에 새로운 Commit을 추가
        * ![Merge](https://media.discordapp.net/attachments/555042542262026262/555315361906688020/merge.PNG)
    * Rebase
        * Master Branch의 최신 Commit까지 갱신 후 최신 Commit에 Slave Branch의  
          Commit(Slave Branch 생성 후 만들어진 Commit)을 추가
        * 작업내역 상 Slave Branch의 시간이 가장 늦음
        * ![Rebase](https://media.discordapp.net/attachments/555042542262026262/555315363986800640/rebase.PNG)
    * 장단점 비교

구분 | merge | rebase   
-- | -- | --  
장점 | 이해가 쉬움 | 단순한 작업내역
-| 원래 Branch의 Commit이 유지 | Commit을 합치는 직관적인 방법  
단점 | 동일한 Branch에서 작업하기에, 작업내역이 번잡 | 충돌 상황 시 복잡   
    
---

#### 5. 기타
* 원격 저장소 연결 확인  
 `git remote -v`
* 저장소 상태 확인  
 `git status`
* Branch 확인  
 `git branch -v`
  
---