Collection_DB Connection
------------------------

---

### Collection Structure

![Collection](https://gitlab.com/Dzerato/study/uploads/ee676ae7e858409c6c73e5da67a9d51a/Collection.png)

-	##### List
	-	데이터의 저장 순서(Index) 존재
	-	중복 데이터를 허용
	-	데이터의 크기 지정X
	-	삽입, 삭제 시 데이터 자동 정렬
	-	##### Vector
	-	데이터의 공간 부족 시 배열 크기 2배로 증가
	-	동기화 기능 제공(다중 스레드에 적합)
	-	##### ArrayList
	-	index를 이용한 빠른 검색
	-	데이터의 추가/삭제 시 낮은 효율
	-	##### LinkedList
	-	데이터의 추가/삭제 시 빠른 처리
	-	데이터 검색 시(index없음, 순차 검색) 느린 검색
-	##### Set
	-	저장 순서 유지X
	-	중복 데이터 허용X
	-	##### SortedSet
	-	정렬 기능
	-	첫, 마지막 원소 검색 기능
	-	##### NavigableSet
		-	역방향 순회 기능
		-	첫, 마지막 원소 삭제 기능
		-	근접 원소 검색(초과/미만)
		-	##### TreeSet
			-	데이터의 중복 X
			-	순서 보장 X
	-	##### HashSet
	-	데이터 중복X
	-	순서 보장X
	-	##### LinkedHashSet
	-	HashSet
	-	Linked

![HashMap](https://gitlab.com/Dzerato/study/uploads/4a21b83bbb63be1790dd7ffd6d19b0e5/Map.png)

-	##### Map

	-	Key와 Value를 쌍으로 가진 집합
	-	중복 허용 X

	-	##### HashMap

		-	Key or Value의 값으로 null 허용
		-	별도 정렬 X
	-	##### HashTable

		-	Key or Value의 값으로 null 비허용
		-	별도 정렬 X
	-	##### TreeMap

		-	Key 값에 대한 정렬

##### ※ Iterator(반복자)

> Iterator<E> iterator()

-	Collection과 Map에서 상속받는 Interface
-	요소의 검색을 위한 메소드
-	interator 메소드를 통해 인스턴스 생성, 인스턴스 참조 값 반환
-	Method

> hasNext() : 다음 요소가 있는 확인(TRUE/FALSE)  
> next() : 다음 요소를 읽어옴  
> remove() : next()로 읽어온 요소를 삭제

-	List_Iterator / Set_Iterator

```
 List list = new ArrayList();
 //HashSet set1 = new HashSet();

 ...
 Iterator <string> itr = list.iterator();  
 //Iterator itr = set1.iterator();

 while(itr.hasNext()){
  String str = itr.next();
  System.out.println(str)
}
```

-	Map_Iterator

```
  Map<String, Integer> hm = new HashMap<String, Interger>();
  ...
  Set<String> keys = hm.keyset();
  // hm 맵에 모든 key 값을 set 컬렉션에 저장

  Iterator<String> itr = keys.iterator)();
  while(it.hashNext()){
    String key = it.next(); // Set의 key값을 하나씩 대입
    int valuye = hm.get(key); // 해당 key 값의 value를 대입
    System.out.println(key + ":" + value);
  }
}
```

---

### DB Connection

```
// MusicJdbcStore.java
private ConnectionFactory factory;

...

// ConnectionFactory.java
private ConnectionFactory() {
  try {
    Class.forName(DRIVER_NAME);
  } catch (ClassNotFoundException e) {
    e.printStackTrace();
  }
}
```

> MusicJdbcStore 클래스 사용 시 DB 연동을 위한 드라이버(API) 사용

```
// MusicJdbcStore.java
public MusicJdbcStore() {
   factory = ConnectionFactory.getInstance();
}

...

// ConnectionFactory.java
private static ConnectionFactory instance;
...
public static ConnectionFactory getInstance() {
  if(instance == null) {
    instance = new ConnectionFactory();
  }
  return instance;
}
```

> MusicJdbcStore 클래스의 객체 생성 시 DB 연결을 위한 싱글톤패턴 객체(factory) 생성

```
// MusicJdbcStore.java
conn = factory.createConnection();

...

// ConnectionFactory.java
private static final String URL = "jdbc:oracle:thin:@localhost:1521:xe";
private static final String USER_NAME = "hr";
private static final String PASSWORD = "hr";
...
public Connection createConnection() throws SQLException{
  return DriverManager.getConnection(URL, USER_NAME, PASSWORD);
}
```

> DB 연결 시 각 연결 설정을 통해 DB 연결 시도

---

### PreparedStatement / Statement

###### PreparedStatement

```
     String sql = "SELECT ID, NAME, ARTIST_NAME, ALBUM_TITLE, IMAGE, AGENT_NAME FROM MUSIC_TB WHERE ID = ?" ;
     ...
     try {
       conn = factory.createConnection();
       pstmt = conn.prepareStatement(sql);
       pstmt.setInt(1, id);
       rs= pstmt.executeQuery();
```

> ?를 통해 입력 SQL문을 만들어두고, 변수를 따로 입력

##### Statement

```
String sql = "SELECT ID, NAME, ARTIST_NAME, ALBUM_TITLE, IMAGE, AGENT_NAME FROM MUSIC_TB WHERE ID = '" + id + "'" ;
...
try {
  conn = factory.createConnection();
  stmt = conn.Statement(sql);
  rs= stmt.executeQuery();
```

##### Resource 반환

```

// MusicJdbcStore.java
  JdbcUtils.close(conn,rs,stmt);
```

> resource 반환을 위한 메소드 호출

```
// JdbcUtils.java
public static void close(AutoCloseable... autoCloseables) {
```

> 가변인자방식을 통해, 매개변수의 수에 상관없이 동일한 메소드 처리

```
  for(AutoCloseable autoCloseable : autoCloseables) {
    if(autoCloseable == null) {
      continue;
    }
    try {
      autoCloseable.close();
    }catch (Exception e) {
      e.printStackTrace();
    }
  }
}
```

> 가변 인자의 값을 하나씩 확인하여 처리

---

### Music Process

![Music Process](https://gitlab.com/Dzerato/study/uploads/5eca332fc9b40b803b41d813bf158be7/Music_%EA%B5%AC%EC%A1%B0%EB%8F%84.png)
