Data Access Process 구조
------------------------

---

### Web Application Architecture

![Acchitecture Process](https://gitlab.com/Dzerato/study/uploads/5937b7c9c437cc5f33c1dd87dd53872d/image.png)

-	비즈니스 로직 : 핵심 업무 처리에 대한 방법 기술
-	도메인 객체 : 각 계층 사이 데이터가 전달되는 실질적인 비즈니스 객체  
	비즈니스 로직이 포함됐다는 점에서 DTO와 차이  
	> DTO(Data Transfer Object) VS Domain Entity  
	> https://funnygangstar.tistory.com/123

---

### Session and Cookie

-	C/S 구조는 비-연결지향 구조로서, 필요시에만 통신을 함
-	항시 연결된듯한 구조를 주기 위해 상태 유지를 위한 정보가 필요

-	cookie

	-	클라이언트의 특정 상태를 유지하기 위한 정보값
	-	서버를 통해 생성된 값은 클라이언트에서 저장
	-	필요 시 클라이언트의 저장된 값은 서버로 재전송됨

	![Cookie](https://gitlab.com/Dzerato/study/uploads/3b252daee434e5586ed7f7d3faeb8697/image.png)

-	Session

	-	Cookie 값이 클라이언트에 저장된다는 점에서 위험
	-	특정 정보를 서버에서 저장하며 세션 객체와 키가 생성
	-	해당 키만을 Cookie를 통해 클라이언트에게 전달
	-	필요 시 클라이언트는 저장된 Cookie의 세션키값을 전달
	-	서버는 해당 세션키값을 대조하여 사용자 정보를 확인

	![session](https://gitlab.com/Dzerato/study/uploads/812edb7b81940ac5dfd540f3761804d6/image.png)

-	Cookie 비 사용시

	-	일반적으로는 Cookie에 세션키값을 저장
	-	Cookie를 사용하지 않는 경우 URL에 지속적으로 세션키값을 추가

---

### DB Connection

-	Connector 생성

```
String jdbc_driver = "com.mysql.jdbc.Driver";
```

> java에서 mysql로 접근하는 기능을 사용(API)

```
String jdbc_url = "jdbc:mysql://localhost:3306/jspdb";
```

> 접근 하고자 하는 DB의 접근 경로 및 이름

```
Class.forName(jdbc_driver);
```

> 접근 기능(드라이버) 확인

```
Connection conn = DriverManager.getConnection(jdbc_url,"root","1234");
```

> 커넥션을 통해 해당 DB에 특정 유저로 접근

-	Query 생성

```
String sql = "select * from student order by 1 asc";
PreparedStatement pstmt = conn.prepareStatement(sql);
```

> sql문을 문자열로 생성 후 이를 prepareStatement로 삽입

-	Result 반환

```
ResultSet rs = pstmt.executeQuery();
```

> Query가 실행되고 나온 결과값을 rs 객체에 저장

```
while(rs.next())
```

> rs 객체의 각 레코드를 순차적으로 확인

-	resource 반환

```
rs.close();
pstmt.close();
conn.close();
```

> 결과값 반환 객체(rs) / sql 실행 객체(pstmt) / DB 연결객체(conn) 각 resource를 DB 사용 후 반환

---

### 상속과 형(Type)

```
Lecture lecture=new Lecture(lectureName, instructorName, lectureIntroduce);
```

> = 을 기준으로 좌형과 우형은 동일한 형태를 지녀야 한다.  
> ex) int = int / string = string

```
  private LectureStore store;
  this.store = LectureStoreLogic.getInstace();
...
public class LectureStoreLogic implements LectureStore{
  ...
}  
```

> 상속받는 경우에 한하여, 우형(Child)는 좌형(Parents)의 형을 사용이 가능하다.  
> 반대, 부모가 자식의 형태를 사용하는 것은 불가능하다.(cating필요)

```
super();
```

> 자식 클래스에서 부모클래스를 상속받을때, 부모클래스 객체를 생성  
> 해당 객체를 통해 자식 클래스에서는 부모클래스의 메소드를 호출이 가능  
> 모든 클래스는 object.class를 상속받음  
> 단, 생성자의 경우 적절한(ex : 매개변수의 차이)형태의 생성자를 사용하지 않는 경우 Error 발생  
> 이 경우 자식 클래스에서 호출한 생성자 형태를 부모 클래스에서 생성 or 자식 클래스의 생성자 호출을 변경

. 상속에서 객체 생성의 차이

![객체의 사용 영역 차이](https://gitlab.com/Dzerato/study/uploads/a33d26b317a11285d3bf87be9fe1fe76/image.png)

> Child 클래스의 메소드 이용여부가 달라짐
